const electron = require('electron');
const { ipcRenderer } = electron;
const list = document.querySelector('#todo-list');

ipcRenderer.on('todo:newTask', (event, task) => {
  const li = document.createElement('li');
  const text = document.createTextNode(task);

  li.classList.add("p-2");
  li.classList.add("m-1");
  li.classList.add("bg-gray-600");
  li.appendChild(text);
  list.appendChild(li);
});

ipcRenderer.on('todo:clear', (event, task) => {
  list.innerHTML = '';
});