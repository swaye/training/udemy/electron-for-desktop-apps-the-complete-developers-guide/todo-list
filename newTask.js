const electron = require('electron');
const { ipcRenderer } = electron;

document.querySelector('#newTask').addEventListener('submit', (event) => {
  event.preventDefault();
  let form = event.target;
  let task = form.querySelector('input').value;
  if (empty(task)) {
    alert('Please Enter a new task');
  }
  ipcRenderer.send('todo:newTask', task);
  
});

function empty(element) {
  if (element == null || element == '') {
    return true;
  }
  return false;
}
