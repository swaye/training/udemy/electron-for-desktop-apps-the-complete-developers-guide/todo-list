const electron = require('electron');

const { app, BrowserWindow, ipcMain, Menu } = electron;

let newTaskWindow;
function createNewTaskWindow() {
  newTaskWindow = new BrowserWindow({
    width: 300,
    height: 200,
    title: 'Add New Task',
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  newTaskWindow.loadURL(`file://${__dirname}/newTask.html`);
  newTaskWindow.on('closed', () => {
    newTaskWindow = null;
  });
}

ipcMain.on('todo:newTask', (event, task) => {
  mainWindow.webContents.send('todo:newTask', task);
  newTaskWindow.close();
})

const menuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'New Todo',
        click() {
          createNewTaskWindow();
        },
      },
      {
        label: 'Clear Todos',
        click() {
          mainWindow.webContents.send('todo:clear');
        },
      },
      {
        label: 'Quit',
        accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
        click() {
          app.quit();
        },
      },
    ],
  },
];

let mainWindow;
app.on('ready', () => {
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  mainWindow.loadURL(`file://${__dirname}/index.html`);
  mainWindow.on('closed', () => app.quit());

  const mainMenu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(mainMenu);
});


if(process.platform === 'darwin') {
  menuTemplate.unshift({ label: app.getName() });
}

if (process.env.NODE_ENV !== 'production') {
  menuTemplate.push({label: 'Dev Tools', submenu: [{role: 'reload'}, {role: 'toggledevtools'}]});
}